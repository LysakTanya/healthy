import "react-native-gesture-handler";
import React, { useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import PillsScreen from "./src/pills/PillsScreen";
import NewsScreen from "./src/news/NewsScreen";
import ScheduleScreen from "./src/schedule/ScheduleScreen";
import Icon from "react-native-vector-icons/FontAwesome";
import { Colors } from "./src/assets/Colors";
import PushNotification from "react-native-push-notification";
import MCIcon from "react-native-vector-icons/MaterialCommunityIcons";
import { ScheduleStackNavigator } from "./src/schedule/StackNavigator";
import { PillsStackNavigator } from "./src/pills/StackNavigator";
import { NewsStackNavigator } from "./src/news/StackNavigator";
import ErrorBoundary from "react-native-error-boundary";
import firestore from "@react-native-firebase/firestore";

const Tab = createMaterialBottomTabNavigator();

export default function App() {
  const errorHandler = (error, stackTrace) => {
    firestore().collection("errors").add({error: error, stackTrace: stackTrace});
  }

  useEffect(() => {
    PushNotification.createChannel(
      {
        channelId: "default", // (required)
        channelName: "Default channel", // (required)
        channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
        playSound: true, // (optional) default: true
        soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
        importance: 4, // (optional) default: 4. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
      },
      (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
  }, []);

  return (
    <NavigationContainer>
      <ErrorBoundary onError={errorHandler}>
        <Tab.Navigator
          initialRouteName="News"
          labeled={false}
          activeColor={Colors.darkGreen}
          barStyle={{ backgroundColor: Colors.lightGreen }}
          inactiveColor={Colors.green}
        >
          <Tab.Screen
            name="Pills"
            component={PillsStackNavigator}
            options={{
              tabBarIcon: ({ color }) => (
                <MCIcon name="pill" size={26} color={color} />
              ),
            }}
          />
          <Tab.Screen
            name="News"
            component={NewsStackNavigator}
            options={{
              tabBarIcon: ({ color }) => (
                <MCIcon
                  name="newspaper-variant-outline"
                  size={26}
                  color={color}
                />
              ),
            }}
          />
          <Tab.Screen
            name="Schedule"
            component={ScheduleStackNavigator}
            options={{
              tabBarIcon: ({ color }) => (
                <MCIcon
                  name="calendar-clock"
                  size={25}
                  color={color}
                  android={"md-calendar"}
                />
              ),
            }}
          />
        </Tab.Navigator>
      </ErrorBoundary>
    </NavigationContainer>
  );
}

import React from "react";
import {
  Text,
  TouchableOpacity,
  View,
  ToastAndroid,
  Linking,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import styles from "./NewsScreenStyles";

const NewsDetails = ({ navigation, route }: any) => {
  const { news } = route.params;

  return (
    <>
      <ScrollView style={styles.container}>
        <View style={styles.detailsContainer}>
          <Text style={styles.detailsHeader}>{news.title}</Text>
          <Text style={styles.detailsSummary}>{news.summary}</Text>
          <Text style={styles.detailsDescription}>{news.body}</Text>
        </View>
        <View style={styles.buttons}>
          <TouchableOpacity
            style={styles.linkButton}
            onPress={() => Linking.openURL(news.link)}
          >
            <Icon name="link-variant" size={25} color={"white"}></Icon>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
};

export default NewsDetails;

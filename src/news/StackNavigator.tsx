import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Text, View } from "react-native";
import { Colors } from "../assets/Colors";
import NewsScreen from "./NewsScreen";
import styles from './NewsScreenStyles';
import NewsDetails from "./NewsDetails";

const Stack = createStackNavigator();

export function NewsStackNavigator() {
  return (
    <Stack.Navigator initialRouteName="News">
      <Stack.Screen
        name="News"
        component={NewsScreen}
        options={{
          headerStyle: styles.header,
          headerTintColor: Colors.darkGreen,
          headerTitleStyle: styles.headerTitle,
        }}
      />
      <Stack.Screen
        name="News details"
        component={NewsDetails}
        options={{
          headerStyle: styles.header,
          headerTintColor: Colors.darkGreen,
          headerTitleStyle: styles.headerTitle,
        }}
      ></Stack.Screen>
    </Stack.Navigator>
  );
}

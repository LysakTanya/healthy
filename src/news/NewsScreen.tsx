import React, { useEffect, useState } from "react";
import {
  Button,
  FlatList,
  RefreshControl,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import styles from "./NewsScreenStyles";
import PushNotification from "react-native-push-notification";
import firestore from "@react-native-firebase/firestore";
import * as Progress from "react-native-progress";
import { Colors } from "../assets/Colors";

interface INews {
  title: string;
  summary: string;
  body: string;
  link: string;
  date: string;
}

const wait = (timeout: number) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const NewsScreen = ({ navigation }: any) => {
  const [news, setNews] = useState<INews[]>([] as INews[]);
  const [refreshing, setRefreshing] = React.useState(false);
  const [loading, setLoading] = useState(false);

  const loadNews = () => {
    setLoading(true);
    firestore()
    .collection("news")
    .get()
    .then((info) => {
      setNews(
        info.docs.map((item) => {
          return {
            title: item.get("title"),
            summary: item.get("summary"),
            body: item.get("body"),
            date: item.get("date"),
            link: item.get("link"),
          };
        })
      );
      setLoading(false);
    });
  }

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    loadNews();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    loadNews();
  }, []);
  
  const handleNewsPress = (item: INews) => {
    navigation.push("News details", { news: item });
  };

  const NewsItem = (props: any) => {
    const { item } = props;
    return (
      <TouchableOpacity
        style={styles.newsItemContainer}
        onPress={() => handleNewsPress(item)}
      >
        <View style={styles.imageContainer}>
          <View style={styles.circle}></View>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.medicineHeader}>{item.title}</Text>
          <Text style={styles.medicineSummary}>
            {String(item.summary).substr(0, 100)}...
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <>
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {loading ? (
          <Progress.Circle
            size={50}
            showsText={true}
            indeterminate={true}
            style={{ justifyContent: "center", alignSelf: "center", flex: 1 }}
            borderColor={Colors.darkGreen}
            borderWidth={4}
          />
        ) : (
          news?.map((item) => <NewsItem item={item} />)
        )}
      </ScrollView>
    </>
  );
};

export default NewsScreen;

export const Colors = {
    lightYellow: "#F6F7CD",
    lightGreen: "#DFE49B",
    green: "#55761A",
    darkGreen: "#264E2C"
}
import React from "react";
import {
  Text,
  TouchableOpacity,
  View,
  ToastAndroid,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import styles from "./PillsScreenStyles";
import RNHTMLtoPDF from "react-native-html-to-pdf";

const PillDetails = ({ navigation, route }: any) => {
  const { medicine } = route.params;

  const showToast = () => {
    ToastAndroid.show(
      "Successfully downloaded to documents folder!",
      ToastAndroid.SHORT
    );
  };

  const createPDF = async () => {
    let options = {
      html: `<h1>${medicine.name}</h1>
      <p><i>${medicine.summary}</i></p>
      <p>${medicine.description}</p>`,
      fileName: medicine.name,
      directory: "Documents",
    };

    let file = await RNHTMLtoPDF.convert(options);
    showToast();
  };

  return (
    <>
      <ScrollView style={styles.container}>
        <View style={styles.detailsContainer}>
          <Text style={styles.detailsHeader}>{medicine.name}</Text>
          <Text style={styles.detailsSummary}>{medicine.summary}</Text>
          <Text style={styles.detailsDescription}>{medicine.description}</Text>
        </View>

          <TouchableOpacity style={styles.downloadButton} onPress={createPDF}>
            <Icon
              name="cloud-download-outline"
              size={30}
              color={"white"}
            ></Icon>
          </TouchableOpacity>

      </ScrollView>
    </>
  );
};

export default PillDetails;

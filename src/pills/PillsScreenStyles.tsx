import { StyleSheet } from "react-native";
import { Colors } from "../assets/Colors";

const PillsScreenStyles = StyleSheet.create({
  container: {
    backgroundColor: Colors.lightYellow,
    flex: 1,
    height: '100%',
  },

  header: {
    height: 60,
    backgroundColor: Colors.lightGreen,
  },

  headerTitle: {
    fontWeight: "700",
    fontSize: 25,
  },

  medicineItemContainer: {
    backgroundColor: "white",
    width: "95%",
    alignSelf: "center",
    margin: 10,
    marginBottom: 0,
    height: 100,
    borderRadius: 15,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },

  imageContainer: {
    backgroundColor: "#9DB33E",
    borderRadius: 15,
    width: 75,
    height: 75,
    margin: 20,
    justifyContent: "center",
  },

  textContainer: { flexDirection: "column", width: 250 },

  medicineHeader: {
    fontWeight: "700",
    fontSize: 16,
    color: Colors.darkGreen,
  },

  medicineSummary: {
    fontWeight: "300",
    fontSize: 13,
    color: Colors.darkGreen,
  },

  circle: {
    backgroundColor: Colors.lightYellow,
    borderRadius: 50,
    width: 25,
    height: 25,
    alignSelf: "center",
  },

  detailsContainer: {
    backgroundColor: "white",
    margin: 20,
    borderRadius: 15,
    width: "90%",
    flexDirection: "column",
  },

  detailsHeader: {
    fontWeight: "700",
    fontSize: 24,
    color: Colors.darkGreen,
    margin: 20,
  },

  detailsSummary: {
    fontWeight: "500",
    fontSize: 20,
    color: Colors.darkGreen,
    margin: 15,
    marginTop: 0,
  },

  detailsDescription: {
    fontWeight: "300",
    fontSize: 16,
    color: Colors.darkGreen,
    margin: 15,
    marginTop: 0,
  },

  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10, 
    height: 70
  },

  downloadButton: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#55761A',
    borderRadius: 10,
    alignSelf: 'center'
  }
});

export default PillsScreenStyles;

import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Text, View } from "react-native";
import { Colors } from "../assets/Colors";
import PillsScreen from "./PillsScreen";
import styles from './PillsScreenStyles';
import PillDetails from "./PillDetails";

const Stack = createStackNavigator();

export function PillsStackNavigator() {
  return (
    <Stack.Navigator initialRouteName="Pills">
      <Stack.Screen
        name="Pills"
        component={PillsScreen}
        options={{
          headerStyle: styles.header,
          headerTintColor: Colors.darkGreen,
          headerTitleStyle: styles.headerTitle,
        }}
      />
      <Stack.Screen
        name="Pill details"
        component={PillDetails}
        options={{
          headerStyle: styles.header,
          headerTintColor: Colors.darkGreen,
          headerTitleStyle: styles.headerTitle,
        }}
      ></Stack.Screen>
    </Stack.Navigator>
  );
}

import React, { useEffect, useState } from "react";
import { RefreshControl, ScrollView, Text, View } from "react-native";
import styles from "./PillsScreenStyles";
import firestore from "@react-native-firebase/firestore";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { Colors } from "../assets/Colors";
import * as Progress from "react-native-progress";

interface IMedicine {
  name: string;
  description: string;
  summary: string;
}

const wait = (timeout: number) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const PillsScreen = ({ navigation }: any) => {
  const [medicines, setMedicines] = useState<IMedicine[]>([] as IMedicine[]);
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = React.useState(false);
  const loadPills = () => {
    firestore()
      .collection("medicines")
      .get()
      .then((info) =>
        setMedicines(
          info.docs.map((item) => {
            return {
              summary: item.get("summary"),
              name: item.get("name"),
              description: item.get("description"),
            };
          })
        )
      );
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    loadPills();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    loadPills();
  }, []);

  const handleMedicinePress = (item: IMedicine) => {
    navigation.push("Pill details", { medicine: item });
  };

  //const f = navigation.s.h;
  const MedicineItem = (props: any) => {
    const { item } = props;
    return (
      <TouchableOpacity
        style={styles.medicineItemContainer}
        onPress={() => handleMedicinePress(item)}
      >
        <View style={styles.imageContainer}>
          <View style={styles.circle}></View>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.medicineHeader}>{item.name}</Text>
          <Text style={styles.medicineSummary}>
            {String(item.summary).substr(0, 100)}...
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {loading ? (
          <Progress.Circle
            size={50}
            showsText={true}
            indeterminate={true}
            style={{ justifyContent: "center", alignSelf: "center", flex: 1 }}
            borderColor={Colors.darkGreen}
            borderWidth={4}
          />
        ) : (
          medicines.map((item) => <MedicineItem item={item} />)
        )}
      </ScrollView>
    </>
  );
};

export default PillsScreen;

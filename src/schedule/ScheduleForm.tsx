import React, { useState } from "react";
import { ScrollView, Text, View, Button, ToastAndroid } from "react-native";
import {
  FlatList,
  TextInput,
  TouchableOpacity,
} from "react-native-gesture-handler";
import styles from "./ScheduleScreenStyles";
import { Picker } from "@react-native-picker/picker";
import { Colors } from "../assets/Colors";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import NotificationItem from "./NotificationItem";
import PushNotification from "react-native-push-notification";

const ScheduleForm = ({ navigation, updateNotifications }: any) => {
  const [pill, setPill] = useState("");
  const [measureType, setMeasureType] = useState("pills");
  const [durationType, setDurationType] = useState("days");
  const [during, setDuring] = useState(false);
  const [after, setAfter] = useState(false);
  const [before, setBefore] = useState(true);
  const [amount, setAmount] = useState(1);
  const [duration, setDuration] = useState(10);
  const [notifications, setNotifications] = useState<Date[]>([
    new Date(),
  ] as Date[]);
  const handlePillChange = (value: any) => {
    setPill(value);
  };

  const changeTime = (value: Date, index: number) => {
    const changed = [...notifications];
    changed.splice(index, 1, value);
    setNotifications(changed);
  };

  const handlePlusPress = () => {
    setNotifications(notifications.concat(new Date()));
  };

  const handleDeletePress = (i: number) => {
    setNotifications(notifications.filter((item, index) => index !== i));
  };

  const handleSave = () => {
    if(pill === ""){
      ToastAndroid.show("Fill pill name, please.", ToastAndroid.SHORT);
      return;
    }

    if(duration == 0){
      ToastAndroid.show("Fill duration field, please.", ToastAndroid.SHORT);
      return;
    }

    if(amount == 0){
      ToastAndroid.show("Fill amount field, please.", ToastAndroid.SHORT);
      return;
    }

    let eatingType: string;
    if (before) {
      eatingType = "before eating";
    } else if (after) {
      eatingType = "after eating";
    } else {
      eatingType = "during eating";
    }

    let uniqueId = 0;
    PushNotification.getScheduledLocalNotifications((value) => {
      const scheduledNotifications = value;
      if (scheduledNotifications.length !== 0) {
        scheduledNotifications.sort((a, b) => Number(a.id) - Number(b.id));
        const lastId: number = Number(
          scheduledNotifications[scheduledNotifications.length - 1].id
        );
        uniqueId = lastId + 1;
        console.log({ uid: uniqueId, lastId: lastId });
      }
      for (let i = 0; i < notifications.length; i++) {
        PushNotification.localNotificationSchedule({
          /* Android Only Properties */
          date: notifications[i],
          allowWhileIdle: true,
          channelId: "default", // (required) channelId, if the channel doesn't exist, notification will not trigger.
          showWhen: true, // (optional) default: true
          autoCancel: false, // (optional) default: true
          largeIcon: "pills_32", // (optional) default: "ic_launcher". Use "" for no large icon.
          largeIconUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
          smallIcon: "pills_25", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
          bigText:
            "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
          subText: pill, // (optional) default: none
          bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
          bigLargeIcon: "ic_launcher", // (optional) default: undefined
          bigLargeIconUrl: "https://www.example.tld/bigicon.jpg", // (optional) default: undefined
          color: "red", // (optional) default: system default
          vibrate: true, // (optional) default: true
          vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
          tag: "some_tag", // (optional) add tag to message
          group: pill,
          groupSummary: true, // (optional) set this notification to be the group summary for a group of notifications, default: false
          ongoing: false, // (optional) set whether this is an "ongoing" notification
          priority: "high", // (optional) set notification priority, default: high
          visibility: "private", // (optional) set notification visibility, default: private
          ignoreInForeground: false, // (optional) if true, the notification will not be visible when the app is in the foreground (useful for parity with how iOS notifications appear). should be used in combine with `com.dieam.reactnativepushnotification.notification_foreground` setting
          shortcutId: "shortcut-id", // (optional) If this notification is duplicative of a Launcher shortcut, sets the id of the shortcut, in case the Launcher wants to hide the shortcut, default undefined
          onlyAlertOnce: false, // (optional) alert will open only once with sound and notify, default: false

          when: null, // (optional) Add a timestamp (Unix timestamp value in milliseconds) pertaining to the notification (usually the time the event occurred). For apps targeting Build.VERSION_CODES.N and above, this time is not shown anymore by default and must be opted into by using `showWhen`, default: null.
          usesChronometer: false, // (optional) Show the `when` field as a stopwatch. Instead of presenting `when` as a timestamp, the notification will show an automatically updating display of the minutes and seconds since when. Useful when showing an elapsed time (like an ongoing phone call), default: false.
          timeoutAfter: null, // (optional) Specifies a duration in milliseconds after which this notification should be canceled, if it is not already canceled, default: null

          messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module.

          actions: ["Yes", "No"], // (Android only) See the doc for notification actions to know more
          invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

          /* iOS only properties */
          category: "", // (optional) default: empty string

          /* iOS and Android properties */
          id: uniqueId,
          title: "Do not forget to take a medicine!", // (optional)
          message: `You should take ${pill} now - ${amount} ${measureType} ${eatingType}`, // (required)
          userInfo: {
            pill: pill,
            amount: amount,
            measureType: measureType,
            eatingType: eatingType,
            time: notifications[i],
          }, // (optional) default: {} (using null throws a JSON value '<null>' error)
          playSound: true, // (optional) default: true
          soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
          number: duration, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
          repeatType: "day", // (optional) Repeating interval. Check 'Repeating Notifications' section for more info.
        });
        uniqueId++;
        setTimeout(() => {}, 500);
      }
    });

    ToastAndroid.show("Successfully added to schedule!", ToastAndroid.SHORT);
    setTimeout(() => {}, 1000);

    navigation.pop();
  };
  const handlePillAmountChange = (e: any) => {
    setAmount(e);
  };

  const handleDurationChange = (e: any) => {
    setDuration(e);
  };

  return (
    <>
      <View style={styles.container}>
        <View>
          <FlatList
            scrollEnabled={true}
            keyExtractor={(item, index) => index.toString()}
            data={notifications}
            renderItem={({ item, index }) => {
              const addButton = Number(index) === 0;
              return (
                <View
                  style={{
                    flexDirection: "row",
                    marginRight: 10,
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <NotificationItem
                    changeTime={(date: Date) => changeTime(date, Number(index))}
                    initial={item}
                  />
                  {addButton === true ? (
                    <TouchableOpacity
                      style={{
                        backgroundColor: "white",
                        width: 55,
                        height: 55,
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius: 15,
                      }}
                      onPress={handlePlusPress}
                    >
                      <Icon name="plus" size={25} color={Colors.green}></Icon>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={{
                        backgroundColor: "white",
                        width: 55,
                        height: 55,
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius: 15,
                      }}
                      onPress={() => handleDeletePress(Number(index))}
                    >
                      <Icon name="close" size={25} color={Colors.green}></Icon>
                    </TouchableOpacity>
                  )}
                </View>
              );
            }}
            ListFooterComponent={
              <TouchableOpacity onPress={handleSave} style={styles.saveButton}>
                <Text style={styles.buttonText}>Save</Text>
              </TouchableOpacity>
            }
            ListHeaderComponent={
              <View>
                <View style={styles.pillInputContainer}>
                  <Text style={styles.pillLabel}>Tablet name</Text>
                  <TextInput
                    onChangeText={handlePillChange}
                    placeholder="Tablet name"
                    inlineImageLeft="pills_32"
                    placeholderTextColor="#DFE49B"
                    style={styles.pillInput}
                  />
                </View>
                <View>
                  <Text style={styles.pillLabel}>Amount {"&"} how long?</Text>
                  <View style={styles.pickers}>
                    <View style={styles.pickerContainer}>
                      <TextInput
                        onChangeText={handlePillAmountChange}
                        inlineImageLeft="pills_32"
                        style={styles.pillCountInput}
                        defaultValue={"1"}
                        keyboardType="numeric"
                      />
                      <Picker
                        style={styles.picker}
                        mode={"dropdown"}
                        dropdownIconColor={Colors.green}
                        onValueChange={(value: any) => setMeasureType(value)}
                      >
                        <Picker.Item label="pills" value="pills" />
                        <Picker.Item label="spoons" value="spoons" />
                        <Picker.Item label="ml" value="ml" />
                        <Picker.Item label="items" value="items" />
                      </Picker>
                    </View>
                    <View style={styles.pickerContainer}>
                      <TextInput
                        onChangeText={handleDurationChange}
                        inlineImageLeft="date_32"
                        style={styles.pillDurationInput}
                        defaultValue={"10"}
                        keyboardType="numeric"
                      />
                      <Picker
                        style={styles.picker}
                        mode={"dropdown"}
                        dropdownIconColor={Colors.green}
                        onValueChange={(value: any) => setDurationType(value)}
                      >
                        <Picker.Item label="days" value="days" />
                        <Picker.Item label="months" value="months" />
                      </Picker>
                    </View>
                  </View>
                </View>
                <View>
                  <Text style={styles.pillLabel}>Food {"&"} pills?</Text>
                  <BouncyCheckbox
                    size={30}
                    fillColor={Colors.green}
                    unfillColor="white"
                    text="before eating"
                    isChecked={before}
                    style={styles.checkbox}
                    iconStyle={{ borderColor: "white" }}
                    textStyle={{
                      fontFamily: "JosefinSans-Regular",
                      color: Colors.darkGreen,
                      fontSize: 18,
                      fontWeight: "500",
                      textDecorationColor: "transparent",
                      textDecorationLine: "none",
                    }}
                    disableBuiltInState
                    onPress={(isChecked: boolean | undefined) => {
                      if (isChecked) return;
                      setAfter(false);
                      setBefore(true);
                      setDuring(false);
                    }}
                  />
                  <BouncyCheckbox
                    size={30}
                    fillColor={Colors.green}
                    unfillColor="white"
                    text="during eating"
                    style={styles.checkbox}
                    isChecked={during}
                    iconStyle={{ borderColor: "white" }}
                    textStyle={{
                      fontFamily: "JosefinSans-Regular",
                      color: Colors.darkGreen,
                      fontSize: 18,
                      fontWeight: "500",
                      textDecorationColor: "transparent",
                      textDecorationLine: "none",
                    }}
                    disableBuiltInState
                    onPress={(isChecked: boolean | undefined) => {
                      if (isChecked) return;
                      setAfter(false);
                      setBefore(false);
                      setDuring(true);
                    }}
                  />
                  <BouncyCheckbox
                    size={30}
                    fillColor={Colors.green}
                    unfillColor="white"
                    text="after eating"
                    style={styles.checkbox}
                    isChecked={after}
                    iconStyle={{ borderColor: "white" }}
                    textStyle={{
                      fontFamily: "JosefinSans-Regular",
                      color: Colors.darkGreen,
                      fontSize: 18,
                      fontWeight: "500",
                      textDecorationColor: "transparent",
                      textDecorationLine: "none",
                    }}
                    disableBuiltInState
                    onPress={(isChecked: boolean | undefined) => {
                      if (isChecked) return;
                      setAfter(true);
                      setBefore(false);
                      setDuring(false);
                    }}
                  />
                </View>
                <Text style={styles.pillLabel}>Notifications</Text>
              </View>
            }
          ></FlatList>
        </View>
      </View>
    </>
  );
};

export default ScheduleForm;

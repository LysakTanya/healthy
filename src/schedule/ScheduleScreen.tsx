import React, { useEffect, useState } from "react";
import {
  RefreshControl,
  SafeAreaView,
  ScrollView,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import ScheduleScreenStyles from "./ScheduleScreenStyles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import PushNotification, {
  PushNotificationScheduledLocalObject,
} from "react-native-push-notification";
import { FlatList } from "react-native-gesture-handler";
import ScheduleItem from "./ScheduleItem";
import { useIsFocused } from "@react-navigation/native";
import moment from "moment";

const wait = (timeout: number) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const ScheduleScreen = ({ navigation }: any) => {
  const handleCreateClick = () => {
    navigation.push("Schedule item", {});
  };

  const [tomorrowNotifications, setTomorrowNotifications] = useState<
    PushNotificationScheduledLocalObject[]
  >([] as PushNotificationScheduledLocalObject[]);
  const [todayNotifications, setTodayNotifications] = useState<
    PushNotificationScheduledLocalObject[]
  >([] as PushNotificationScheduledLocalObject[]);
  const isFocused = useIsFocused();
  useEffect(() => {
    updateNotifications();
  }, [navigation, isFocused]);

  const updateNotifications = () => {
    PushNotification.getScheduledLocalNotifications((value) => {
      setTomorrowNotifications(
        value.filter((item) =>
          compareDates(
            moment().add(3, "hours").toDate(),
            moment(item.date).add(3, "hours").toDate()
          )
        ).sort((f, s) => compareDates(f.date, s.date) ? 1 : -1)
      );
      setTodayNotifications(
        value.filter((item) =>
          compareDates(
            moment(item.date).add(3, "hours").toDate(),
            moment().add(3, "hours").toDate()
          )
        ).sort((f, s) => compareDates(f.date, s.date) ? 1 : -1)
      );
    });
  };

  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    updateNotifications();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  const compareDates = (first: Date, second: Date) => {
    if (first.getHours() > second.getHours()) {
      return true;
    } else if (first.getHours() < second.getHours()) {
      return false;
    } else if (first.getMinutes() > second.getMinutes()) {
      return true;
    } else if (first.getMinutes() < second.getMinutes()) {
      return false;
    }

    return false;
  };

  return (
    <>
      <ScrollView
        style={ScheduleScreenStyles.container}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {todayNotifications.length !== 0 && (
          <>
            <Text style={ScheduleScreenStyles.headerDay}>Today</Text>
            {todayNotifications.map((item) => (
              <ScheduleItem
                data={item.data}
                id={item?.id}
                updateNotifications={updateNotifications}
              />
            ))}
          </>
        )}
        {tomorrowNotifications.length !== 0 && (
          <>
            <Text style={ScheduleScreenStyles.headerDay}>Tomorrow</Text>
            {tomorrowNotifications.map((item) => (
              <ScheduleItem
                data={item.data}
                id={item?.id}
                updateNotifications={updateNotifications}
              />
            ))}
          </>
        )}
        {tomorrowNotifications.length === 0 && todayNotifications.length === 0 && (
          <Text style={ScheduleScreenStyles.noNotifications}>
            There are no notifications yet.
          </Text>
        )}
        <TouchableOpacity style={ScheduleScreenStyles.addButton}>
          <Icon
            name="calendar-plus"
            size={35}
            onPress={handleCreateClick}
            color={"white"}
          />
        </TouchableOpacity>
      </ScrollView>
    </>
  );
};

export default ScheduleScreen;

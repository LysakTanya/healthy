import React, { useEffect, useState } from "react";
import { Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Colors } from "../assets/Colors";
import styles from "./ScheduleScreenStyles";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { TouchableOpacity } from "react-native-gesture-handler";
import moment from "moment";

const NotificationItem = (props: any) => {
  const [visible, setVisible] = useState(false);
  const [selectedTime, setSelectedTime] = useState(new Date());
  const handleTimePickerPress = () => {
    setVisible(!visible);
  };

  const onConfirm = (value: any) => {
    setVisible(false);
    setSelectedTime(value);
  };

  const onCancel = () => {
    setVisible(false);
  };

  useEffect(() => {
    props.changeTime(selectedTime);
  }, [selectedTime]);

  return (
    <>
      <TouchableOpacity
        style={styles.notificationContainer}
        onPress={handleTimePickerPress}
      >
        <Icon
          name="alarm-plus"
          color={Colors.green}
          size={35}
          style={{ padding: 10 }}
        />
        <Text>{moment(selectedTime).format("HH:mm a")}</Text>
        {visible && (
          <DateTimePickerModal
            mode="time"
            onConfirm={onConfirm}
            isVisible={visible}
            textColor={Colors.darkGreen}
            onCancel={onCancel}
          />
        )}
      </TouchableOpacity>
    </>
  );
};

export default NotificationItem;

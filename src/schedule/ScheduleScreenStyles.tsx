import { StyleSheet } from "react-native";
import { white } from "react-native-paper/lib/typescript/styles/colors";
import { Colors } from "../assets/Colors";

const ScheduleScreenStyles = StyleSheet.create({
  container: {
    backgroundColor: Colors.lightYellow,
    flex: 1,
  },

  addButton: {
    backgroundColor: Colors.green,
    width: 72,
    height: 59,
    borderRadius: 10,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    margin: 10,
  },

  header: {
    height: 60,
    backgroundColor: Colors.lightGreen,
  },
  headerTitle: {
    fontWeight: "700",
    fontSize: 25,
  },

  pillInput: {
    backgroundColor: "white",
    borderRadius: 15,
    color: Colors.darkGreen,
    fontSize: 14,
    margin: 10,
    padding: 15,
  },

  pillCountInput: {
    backgroundColor: "white",
    borderRadius: 15,
    color: Colors.darkGreen,
    fontSize: 14,
    paddingLeft: 10,
    width: 70,
    textAlign: "center",
  },

  pillDurationInput: {
    backgroundColor: "white",
    borderRadius: 15,
    color: Colors.darkGreen,
    fontSize: 14,
    paddingLeft: 10,
    width: 70,
    textAlign: "center",
  },

  pillLabel: {
    color: Colors.darkGreen,
    fontSize: 20,
    marginLeft: 25,
    marginTop: 5,
    fontWeight: "bold",
  },

  pillInputContainer: {
    marginBottom: 10,
  },

  picker: {
    color: Colors.darkGreen,
    fontSize: 20,
    width: 135,
  },

  pickerContainer: {
    backgroundColor: "white",
    borderRadius: 15,
    width: 200,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 10,
    margin: 10,
  },

  pickers: {
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
  },

  checkbox: {
    fontSize: 18,
    marginLeft: 10,
    marginTop: 10,
    textDecorationColor: "transparent",
    textDecorationLine: "none",
  },

  notificationContainer: {
    backgroundColor: "white",
    width: 314,
    height: 54,
    borderRadius: 15,
    margin: 10,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },

  saveButton: {
    backgroundColor: Colors.green,
    borderRadius: 15,
    width: 100,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    marginBottom: 5,
  },

  buttonText: {
    color: "white",
    fontSize: 20,
    fontWeight: "700",
  },

  scheduleItemContainer: {
    backgroundColor: "white",
    width: 360,
    height: 65,
    borderRadius: 15,
    margin: 10,
    marginTop: 0,
    color: Colors.darkGreen,
    flexDirection: "row",
    justifyContent: "flex-start",
  },

  inlineBlock: {
    backgroundColor: "#9DB33E",
    color: Colors.darkGreen,
    width: 300,
    height: 65,
    borderRadius: 15,
    flex: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  inlineDateBlock: {
    backgroundColor: "white",
    color: Colors.darkGreen,
    flex: 1,
    height: 65,
    borderRadius: 15,
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
  },

  dateText: {
    color: Colors.darkGreen,
    fontSize: 24,
    fontWeight: "700",
  },

  scheduleItemHeading: {
    color: Colors.darkGreen,
    fontSize: 25,
    fontWeight: "700",
    margin: 10,
    marginBottom: 0,
  },

  additionalText: {
    color: Colors.darkGreen,
    fontSize: 14,
    fontWeight: "200",
    marginLeft: 10,
    marginBottom: 10,
  },
  headerDay: {
    color: Colors.darkGreen,
    fontSize: 25,
    margin: 15,
    fontWeight: "700",
  },
  noNotifications: {
    alignSelf: "center",
    fontSize: 20,
    margin: 15,
    fontWeight: "300",
    color: Colors.darkGreen,
  },
});

export default ScheduleScreenStyles;

import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ScheduleScreen from "./ScheduleScreen";
import ScheduleForm from "./ScheduleForm";
import { Text, View } from "react-native";
import ScheduleScreenStyles from "./ScheduleScreenStyles";
import { Colors } from "../assets/Colors";

const Stack = createStackNavigator();

export function ScheduleStackNavigator() {
  return (
    <Stack.Navigator initialRouteName="Schedule">
      <Stack.Screen
        name="Schedule"
        component={ScheduleScreen}
        options={{
          headerStyle: ScheduleScreenStyles.header,
          headerTintColor: Colors.darkGreen,
          headerTitleStyle: ScheduleScreenStyles.headerTitle,
        }}
      />
      <Stack.Screen
        name="Schedule item"
        component={ScheduleForm}
        options={{
          headerStyle: ScheduleScreenStyles.header,
          headerTintColor: Colors.darkGreen,
          headerTitleStyle: ScheduleScreenStyles.headerTitle,
        }}
      ></Stack.Screen>
    </Stack.Navigator>
  );
}

import moment from "moment";
import React from "react";
import { Text, ToastAndroid, TouchableOpacity, View } from "react-native";
import PushNotification from "react-native-push-notification";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Colors } from "../assets/Colors";
import styles from "./ScheduleScreenStyles";

const ScheduleItem = (props: any) => {
  const { data, id } = props;

  return (
    <>
      <View style={styles.scheduleItemContainer}>
        <View style={styles.inlineDateBlock}>
          <Text style={styles.dateText}>
            {moment(data?.time ?? new Date()).format("HH:mm")}
          </Text>
        </View>
        <View style={styles.inlineBlock}>
          <View style={{ flexDirection: "column" }}>
            <Text style={styles.scheduleItemHeading}>{data?.pill}</Text>
            <Text style={styles.additionalText}>
              {data?.amount} {data?.measureType} {data?.eatingType}
            </Text>
          </View>

          <View>
            <TouchableOpacity
              style={{
                backgroundColor: "transparent",
                width: 55,
                height: 55,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 15,
              }}
              onPress={() => {
                PushNotification.cancelLocalNotifications({id: ''+id});
                ToastAndroid.show("Successfully removed from schedule!", ToastAndroid.SHORT);
                props.updateNotifications();
              }}
            >
              <Icon name="close" size={25} color="white" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};

export default ScheduleItem;
